// Arduino code for controlling Lachie's top hat (2nd edition)
#define FASTLED_ALLOW_INTERRUPTS 0
#include "FastLED.h"

#define DATA_PIN    6 //TODO: alter to 7
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    176

// Instantiate color array object, which is the code-representation of the
// physical pixel strip.
CRGB leds[NUM_LEDS];

#define BRIGHTNESS         128
#define FRAMES_PER_SECOND  120
#define HUE_CYCLER_MILLISECONDS  20
#define PATTERN_CYCLER_SECONDS  30
#define PALETTE_CYCLER_SECONDS  45
#define PATTERN_SLOWER_MILLISECONDS  15

// Mapping constants
#define TRI_A          28
#define TRI_B          33
#define TRI_C          21
#define TRI_D          32
#define TRI_E          39
#define TRI_F          23

#define N_TRIANGLES          6

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

// Maps of continuous linear physical pixel arrays
// uint8_t triangleA[TRI_A] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27};
// uint8_t triangleB[TRI_B] = {28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60};
// uint8_t triangleC[TRI_C] = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81};
// uint8_t triangleD[TRI_D] = {82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115};
// uint8_t triangleE[TRI_E] = {116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154};
// uint8_t triangleF[TRI_F] = {155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177};

#define N_VERTICAL_LEVELS 13
#define MAX_VERTICAL_LEVEL_WIDTH 22

const uint8_t verticalLevelWidths[] = {5, 16, 22, 20, 21, 20, 18, 18, 14, 12, 5, 3, 2};

// NOTE: Reference
// uint8_t level0[] = {0, 67, 68, 69, 70}; //5
// uint8_t level1[] = {1, 7, 13, 18, 22, 25, 27, 65, 66, 71, 96, 97, 107, 108, 112, 151}; //16
// uint8_t level2[] = {2, 8, 14, 19, 23, 26, 72, 78, 81, 95, 98, 106, 109, 111, 113, 148, 149, 150, 152, 167, 168, 175}; //22
// uint8_t level3[] = {3, 9, 15, 20, 24, 64, 73, 77, 82, 94, 99, 105, 110, 114, 146, 147, 153, 166, 169, 174}; //20
// uint8_t level4[] = {4, 10, 16, 21, 63, 74, 76, 79, 83, 93, 100, 104, 115, 116, 129, 130, 145, 154, 165, 170, 173}; //21
// uint8_t level5[] = {5, 11, 17, 43, 44, 45, 62, 75, 80, 84, 92, 101, 103, 117, 128, 131, 144, 155, 164, 171}; //20
// uint8_t level6[] = {6, 12, 28, 39, 40, 41, 42, 61, 85, 91, 102, 118, 127, 132, 139, 156, 163, 172}; //18
// uint8_t level7[] = {29, 38, 46, 54, 55, 59, 60, 86, 90, 119, 126, 133, 138, 140, 142, 143, 157, 162}; //18
// uint8_t level8[] = {30, 47, 53, 56, 58, 87, 89, 120, 125, 134, 137, 141, 158, 161}; //14
// uint8_t level9[] = {31, 37, 48, 52, 57, 88, 121, 124, 135, 136, 159, 160}; //12
// uint8_t level10[] = {32, 49, 51, 122, 123}; //5
// uint8_t level11[] = {33, 36, 50}; //3
// uint8_t level12[] = {34, 35}; //2

// Rectangularized coordinate array
const uint8_t verticalCoordinates[][MAX_VERTICAL_LEVEL_WIDTH] = {
        {0, 67, 68, 69, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //5
        {1, 7, 13, 18, 22, 25, 27, 65, 66, 71, 96, 97, 107, 108, 112, 151, 0, 0, 0, 0, 0, 0}, //16
        {2, 8, 14, 19, 23, 26, 72, 78, 81, 95, 98, 106, 109, 111, 113, 148, 149, 150, 152, 167, 168, 175}, //22
        {3, 9, 15, 20, 24, 64, 73, 77, 82, 94, 99, 105, 110, 114, 146, 147, 153, 166, 169, 174, 0, 0}, //20
        {4, 10, 16, 21, 63, 74, 76, 79, 83, 93, 100, 104, 115, 116, 129, 130, 145, 154, 165, 170, 173, 0}, //21
        {5, 11, 17, 43, 44, 45, 62, 75, 80, 84, 92, 101, 103, 117, 128, 131, 144, 155, 164, 171, 0, 0}, //20
        {6, 12, 28, 39, 40, 41, 42, 61, 85, 91, 102, 118, 127, 132, 139, 156, 163, 172, 0, 0, 0, 0}, //18
        {29, 38, 46, 54, 55, 59, 60, 86, 90, 119, 126, 133, 138, 140, 142, 143, 157, 162, 0, 0, 0, 0}, //18
        {30, 47, 53, 56, 58, 87, 89, 120, 125, 134, 137, 141, 158, 161, 0, 0, 0, 0, 0, 0, 0, 0}, //14
        {31, 37, 48, 52, 57, 88, 121, 124, 135, 136, 159, 160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //12
        {32, 49, 51, 122, 123, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //5
        {33, 36, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //3
        {34, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} //2
};

// NOTE: Triangle A is 0-27, B is 28-60, C is 61-80, D is 81-112, E is 113-151, F is 152-175

const uint8_t A_endpoint = TRI_A;
const uint8_t B_endpoint = A_endpoint + TRI_B;
const uint8_t C_endpoint = B_endpoint + TRI_C;
const uint8_t D_endpoint = C_endpoint + TRI_D;
const uint8_t E_endpoint = D_endpoint + TRI_E;
const uint8_t F_endpoint = E_endpoint + TRI_F;

// Palette definitions:
// Gradient palette "autumnrose_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/autumnrose.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 32 bytes of program space.

DEFINE_GRADIENT_PALETTE( autumnrose_gp ) {
        0,  71,  3,  1,
        45, 128,  5,  2,
        84, 186, 11,  3,
        127, 215, 27,  8,
        153, 224, 69, 13,
        188, 229, 84,  6,
        226, 242, 135, 17,
        255, 247, 161, 79
};

// Gradient palette "bambooblossom_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/bambooblossom.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 12 bytes of program space.

DEFINE_GRADIENT_PALETTE( bambooblossom_gp ) {
        0,  13, 44, 11,
        127, 215, 108, 151,
        255,  79, 22, 44
};

// Gradient palette "beautifuldreams_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/beautifuldreams.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 28 bytes of program space.

DEFINE_GRADIENT_PALETTE( beautifuldreams_gp ) {
        0,  27, 27, 49,
        51, 126, 104, 138,
        76,  64, 85, 133,
        127,   4, 16, 78,
        178,  64, 85, 133,
        204, 126, 104, 138,
        255,  27, 27, 49
};

// Gradient palette "blackhorse_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/blackhorse.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( blackhorse_gp ) {
        0, 8, 30, 1,
        53, 29, 99, 237,
        112, 1,  1, 14,
        168, 21,  5, 21,
        219, 1, 5, 2,
        255, 8, 30, 1
};

// Gradient palette "bluefly_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/bluefly.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( bluefly_gp ) {
        0,   0,  0,  0,
        63,   0, 39, 64,
        191, 175,215,235,
        255,   0,  0,  0
};

// Gradient palette "carousel_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/carousel.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 28 bytes of program space.

DEFINE_GRADIENT_PALETTE( carousel_gp ) {
        0,   2,  6, 37,
        101,   2,  6, 37,
        122, 177,121,  9,
        127, 217,149,  2,
        132, 177,121,  9,
        153,  84, 13, 36,
        255,  84, 13, 36
};

// Gradient palette "catfairy_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/catfairy.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( catfairy_gp ) {
        0,  74,124, 85,
        76, 152,128, 44,
        127, 144, 97, 96,
        178, 100, 72,102,
        232,  78, 90,122,
        255,  78, 90,122
};

// Gradient palette "celticsun_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/celticsun.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 8 bytes of program space.

DEFINE_GRADIENT_PALETTE( celticsun_gp ) {
        0, 120, 19,  8,
        255, 255, 97,  5
};

// Gradient palette "cloud_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/cloud.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 12 bytes of program space.

DEFINE_GRADIENT_PALETTE( cloud_gp ) {
        0, 247,149, 91,
        127, 208, 32, 71,
        255,  42, 79,188
};

// Gradient palette "daisyfae_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/daisyfae.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( daisyfae_gp ) {
        0,   4,  8,  0,
        86,  74, 91,  1,
        178, 161,175, 41,
        255, 237,139,  0
};

// Gradient palette "fireandice_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/fireandice.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 24 bytes of program space.

DEFINE_GRADIENT_PALETTE( fireandice_gp ) {
        0,  80,  2,  1,
        51, 206, 15,  1,
        101, 242, 34,  1,
        153,  16, 67,128,
        204,   2, 21, 69,
        255,   1,  2,  4
};

// Gradient palette "otis_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/otis.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 16 bytes of program space.

DEFINE_GRADIENT_PALETTE( otis_gp ) {
        0,  26,  1, 89,
        127,  17,193,  0,
        216,   0, 34, 98,
        255,   0, 34, 98
};

// Gradient palette "parrot_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/parrot.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 20 bytes of program space.

DEFINE_GRADIENT_PALETTE( parrot_gp ) {
        0, 126,  0,  1,
        114, 197,168, 16,
        140, 197,168, 16,
        216,   0,  2, 32,
        255,   0,  2, 32
};

// Gradient palette "rainbow_gp", originally from
// http://soliton.vm.bytemark.co.uk/pub/cpt-city/rc/tn/rainbow.png.index.html
// converted for FastLED with gammas (2.6, 2.2, 2.5)
// Size: 48 bytes of program space.

DEFINE_GRADIENT_PALETTE( rainbow_gp ) {
        0, 126,  1,142,
        25, 171,  1, 26,
        48, 224,  9,  1,
        71, 237,138,  1,
        94,  52,173,  1,
        117,   1,201,  1,
        140,   1,211, 54,
        163,   1,124,168,
        186,   1,  8,149,
        209,  12,  1,151,
        232,  12,  1,151,
        255, 171,  1,190
};

const TProgmemRGBGradientPalettePtr gPalettes[] = {
        autumnrose_gp,
        bambooblossom_gp,
        beautifuldreams_gp,
        blackhorse_gp,
        bluefly_gp,
        carousel_gp,
        catfairy_gp,
        celticsun_gp,
        cloud_gp,
        daisyfae_gp,
        fireandice_gp,
        otis_gp,
        parrot_gp,
        rainbow_gp
};

void setup() {
        delay(200);
        // tell FastLED about the LED strip configuration
        FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
        FastLED.setBrightness(BRIGHTNESS);
        FastLED.setDither(0); // Attempt to reduce flickering at low brightness
        FastLED.clear();
}

// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = {
        sinelonTriangle,     //satisfactory
        sinelon, //satisfactory
        // triangleAlternator, //flickery
        pulsar, //satisfactory
        // wholeTriangleGlitter,         //flickery
        // verticalWipe, //rubbish; redo matrix
        // wholeTriangleGlitter,         //flickery
        testTriangleSolidPalette,  //satisfactory
        // rainbowZoomer, //obnoxious
        // wholeTriangleGlitter, //flickery
        // paletteZoomer, //obnoxious
        testTriangleSolid, //satisfactory
        // wholeTriangleGlitter,         //flickery
        sinelon, //satisfactory
        testTriangleSolidPalette,  //satisfactory
        // wholeTriangleGlitter,         //flickery
        rainbow, //satisfactory
        rainbowWithGlitter, //satisfactory
        // addGlitterWhite, //satisfactory
        // wholeTriangleGlitter,         //flickery
        sinelon, //satisfactory
        testTriangleSolidPalette,  //satisfactory
        // wholeTriangleGlitter,         //flickery
        confetti, //satisfactory
        testTriangleSolidPalette,  //satisfactory
        // wholeTriangleGlitter,         //flickery
        juggle, //satisfactory
        // wholeTriangleGlitter,         //flickery
        testTriangleSolidPalette,  //satisfactory
        // wholeTriangleGlitter,         //flickery
        bpm //satisfactory
        // wholeTriangleGlitter         //flickery
};

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gCurrentPaletteNumber = 0; // Index number of which palette is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

const uint8_t gPatternsSize = ARRAY_SIZE(gPatterns);
const uint8_t gPalettesSize = ARRAY_SIZE(gPalettes);

CRGBPalette16 currentPalette = gPalettes[gCurrentPaletteNumber];

void loop() {
        // Set the color palette for this iteration of the loop
        // currentPalette = gPalettes[gCurrentPaletteNumber];
        // Call the current pattern function once, updating the 'leds' array
        gPatterns[gCurrentPatternNumber]();

        // send the 'leds' array out to the actual LED strip
        FastLED.show();
        // insert a delay to keep the framerate modest
        FastLED.delay(1000/FRAMES_PER_SECOND);

        // do some periodic updates
        EVERY_N_MILLISECONDS(HUE_CYCLER_MILLISECONDS) {
                gHue++; // slowly cycle the "base color" through the rainbow
        }

        EVERY_N_SECONDS(PATTERN_CYCLER_SECONDS) {
                nextPattern(); // change patterns periodically
        }

        EVERY_N_SECONDS(PALETTE_CYCLER_SECONDS) {
                nextPalette(); // change palettes periodically
        }
}

void nextPattern()
{
        // add one to the current pattern number, and wrap around at the end
        gCurrentPatternNumber = (gCurrentPatternNumber + 1) % gPatternsSize;
}

void nextPalette()
{
        // add one to the current pattern number, and wrap around at the end
        gCurrentPaletteNumber = (gCurrentPaletteNumber + 1) % gPalettesSize;
        currentPalette = gPalettes[gCurrentPaletteNumber];
}

void rainbow()
{
        // FastLED's built-in rainbow generator
        fill_rainbow(leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter()
{
        // built-in FastLED rainbow, plus some random sparkly glitter
        rainbow();
        addGlitter(80);
}

void addGlitter(fract8 chanceOfGlitter)
{
        if(random8() < chanceOfGlitter) {
                leds[random8(NUM_LEDS)] += CRGB::White;
        }
}

void rainbowGlitter()
{
        fadeToBlackBy(leds, NUM_LEDS, 32);
        leds[random8(NUM_LEDS)].setHue(gHue);
        FastLED.delay(PATTERN_SLOWER_MILLISECONDS);
}

void addGlitterWhite()
{
        fadeToBlackBy(leds, NUM_LEDS, 32);
        leds[random8(NUM_LEDS)] += CRGB::White;
        FastLED.delay(PATTERN_SLOWER_MILLISECONDS);
}

void confetti()
{
        // random colored speckles that blink in and fade smoothly
        fadeToBlackBy(leds, NUM_LEDS, 32);
        int pos = random8(NUM_LEDS);
        leds[pos] += CHSV(gHue + random8(64), 200, 255);
}

void sinelon()
{
        // a colored dot sweeping back and forth, with fading trails
        fadeToBlackBy(leds, NUM_LEDS, 32);
        int pos = beatsin8(13, 0, NUM_LEDS-1);
        leds[pos] += CHSV(gHue, 255, 192);
}

void bpm()
{
        // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
        uint8_t BeatsPerMinute = 62;
        CRGBPalette16 palette = PartyColors_p;
        uint8_t beat = beatsin8(BeatsPerMinute, 64, 255);
        for(int i = 0; i < NUM_LEDS; i++) { //9948
                leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
        }
}

void juggle()
{
        // eight colored dots, weaving in and out of sync with each other
        fadeToBlackBy(leds, NUM_LEDS, 32);
        byte dothue = 0;
        for(int i = 0; i < 8; i++) {
                leds[beatsin8(i+7, 0, NUM_LEDS-1)] |= CHSV(dothue, 200, 255);
                dothue += 32;
        }
}

void testTriangleSolid()
{
        fill_solid(&(leds[0]), TRI_A, CHSV(gHue, 255, 255));
        fill_solid(&(leds[A_endpoint]), TRI_B, CHSV(gHue + 43, 255, 255));
        fill_solid(&(leds[B_endpoint]), TRI_C, CHSV(gHue + 85, 255, 255));
        fill_solid(&(leds[C_endpoint]), TRI_D, CHSV(gHue + 128, 255, 255));
        fill_solid(&(leds[D_endpoint]), TRI_E, CHSV(gHue + 170, 255, 255));
        fill_solid(&(leds[E_endpoint]), TRI_F, CHSV(gHue + 213, 255, 255));
}

void testTriangleSolidPalette()
{
        fill_solid(&(leds[0]), TRI_A, ColorFromPalette(currentPalette, gHue));
        fill_solid(&(leds[A_endpoint]), TRI_B, ColorFromPalette(currentPalette, gHue + 43));
        fill_solid(&(leds[B_endpoint]), TRI_C, ColorFromPalette(currentPalette, gHue + 85));
        fill_solid(&(leds[C_endpoint]), TRI_D, ColorFromPalette(currentPalette, gHue + 128));
        fill_solid(&(leds[D_endpoint]), TRI_E, ColorFromPalette(currentPalette, gHue + 170));
        fill_solid(&(leds[E_endpoint]), TRI_F, ColorFromPalette(currentPalette, gHue + 213));
}

void wholeTriangleGlitter()
{
        fadeToBlackBy(leds, NUM_LEDS, 32);
        if (random8() < 96) {
                triangleFill(random8(N_TRIANGLES), ColorFromPalette(currentPalette, gHue));
        }
        FastLED.delay(PATTERN_SLOWER_MILLISECONDS);
}

void pulsar()
{
        int brightness = beatsin8(13, 32, 255);
        fill_solid(leds, NUM_LEDS, ColorFromPalette(currentPalette, gHue, brightness));
}

void verticalWipe()
{
        fadeToBlackBy(leds, NUM_LEDS, 128);
        int pos = beatsin8(13, 0, N_VERTICAL_LEVELS-1);
        for (uint8_t i = 0; i < verticalLevelWidths[pos]; i++) {
                leds[verticalCoordinates[pos][i]] += ColorFromPalette(currentPalette, gHue);
        }
}

void rainbowZoomer()
{
        fill_rainbow(leds, NUM_LEDS, gHue, gHue%8);
        FastLED.delay(PATTERN_SLOWER_MILLISECONDS*2);
}

void paletteZoomer()
{
        fill_palette(leds, NUM_LEDS, gHue, gHue%8, currentPalette, 255, LINEARBLEND);
        FastLED.delay(PATTERN_SLOWER_MILLISECONDS*2);
}

void sinelonTriangle()
{
        // a colored dot sweeping back and forth, with fading trails
        fadeToBlackBy(leds, NUM_LEDS, 64);
        int pos = beatsin8(10, 0, N_TRIANGLES);
        triangleFill(pos, CHSV(gHue, 255, 192));
}

void triangleAlternator()
{
        fadeToBlackBy(leds, NUM_LEDS, 8);
        if (gHue == 0) {
                triangleFill(0, ColorFromPalette(currentPalette, gHue));
                triangleFill(2, ColorFromPalette(currentPalette, gHue + 85));
                triangleFill(4, ColorFromPalette(currentPalette, gHue + 170));
        } else if (gHue == 128) {
                triangleFill(1, ColorFromPalette(currentPalette, gHue));
                triangleFill(3, ColorFromPalette(currentPalette, gHue + 85));
                triangleFill(5, ColorFromPalette(currentPalette, gHue + 170));
        }
}

void triangleFill(uint8_t tri, CRGB color)
{
        switch (tri) {
        case 0:
                fill_solid(&(leds[0]), TRI_A, color);
                break;
        case 1:
                fill_solid(&(leds[A_endpoint]), TRI_B, color);
                break;
        case 2:
                fill_solid(&(leds[B_endpoint]), TRI_C, color);
                break;
        case 3:
                fill_solid(&(leds[C_endpoint]), TRI_D, color);
                break;
        case 4:
                fill_solid(&(leds[D_endpoint]), TRI_E, color);
                break;
        case 5:
                fill_solid(&(leds[E_endpoint]), TRI_F, color);
                break;
        default:
                break;
        }
}
